package dawson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    public void thisIsATestMethod()
    {
        assertEquals("Checks if method echo in app is valid.",5,App.echo(5));    
    }
    @Test
    public void ThisIsTheThirdTest()
    {
        assertEquals("This is suppose to fail.",5,App.oneMore(5));
    }
}
